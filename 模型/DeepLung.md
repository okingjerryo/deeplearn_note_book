>  Reference: DeepLung: Deep 3D Dual Path Nets for Automated Pulmonary Nodule Detection and Classification

# 一 、总体框架

* Nodule detection
    * 3D dual path Network (降低参数)<sup>[1]<sup>
    * 3D Faster R-CNN
        * 3D conv kernels
    * UNet EDcoder
* Classification
    * 3D dual path network features
* dataset
    * main:LIDC-IDR
    * Related: LUNA16、
# 二、思路


* 为了能预览3DCT，制作了2个3DConvNet。如上
* 3DNet 参数太多，使用DualPath Network降参
* 从[Faster RCNN](https://arxiv.org/abs/1506.01497)启发，使用了3D FRCNN做检测
    * 3D ConvNets 去错。重视查全率
    * 3D FRCNN 后接EDcoder
* 3D DPN做nodule 检测
# 三 、肿瘤检测结构

1. UNet-like encoder-decoder structure ,contains 3D dual path block
        图中方框对应候选框：#slices(z)*#rows(x)*#cols(y)*#maps
        图中连接的格式：filter  #slices*#rows*#cols
2. DPN block结构：
            相关链接：DPN结构

3. 反卷积<sup>[2]</sup>

# 四 、良恶检测

* 好坏检测时需要系统学精细级的特征。
* 输入时先crop 96* 96* 96 的 然后 每一个检测出的区域再在中心crop 48* 48* 48 的
* 3D avg pooling和二分logistic Regression 
# 五、Loss

* 候选框loss思路
    * classiﬁcation loss :当前框是否为肿瘤，使用cross entorpy
    * regression loss ：当前坐标x，y，z 与肿瘤直径d 的回归，使用平滑L1 Loss
    * IoU[3] >0.5 p* = 1(正框)
    * IoU <0.02 p* = 2 (负框)
     loss 数学表达式：
                
                通常取0.5
    其中：
        
    关于groundtruth

---    
### 相关工作：
- [3D fully ConvNet (FCN)](https://arxiv.org/abs/1606.04797)

---
### 参考文献
- [[1] DPN（Dual Path Network）算法详解 - CSDN博客](https://blog.csdn.net/u014380165/article/details/75676216)
- [[2] Deconvolutional networks](http://www.matthewzeiler.com/wp-content/uploads/2017/07/cvpr2010.pdf)

